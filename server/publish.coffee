Meteor.publish null, ->
  Meteor.users.find()
Meteor.publish 'projects', ->
  db.Projects.find()
Meteor.publishComposite 'project', (id) ->
  {
    find: ->
      db.Projects.find id
    children: [ { find: (project) ->
      db.Submissions.find _id: $in: project.submissions
 } ]
  }
