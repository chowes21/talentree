Meteor.methods
  'toggleSelectSubmission': (projectId, userId, isSelected) ->
    db.Projects.update {
        _id: projectId
        'submissions.user': userId
      }, {
          '$set': {
            'submissions.$.selected': isSelected
          }
      }, (err, affectedDocs) ->
        console.log err, affectedDocs