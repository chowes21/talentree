AccountsTemplates.configure
  forbidClientAccountCreation: false
  onLogoutHook: ->
    Router.go '/login'
    return
  onSubmitHook: (error, state) ->
    if !error
      if state == 'signUp'
        Router.go '/complete-signup'
      else
        Router.go '/'
    return
