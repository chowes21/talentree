Template.registerHelper 'dateFormat', (ts) ->
  moment(ts).format 'D/MM/YYYY'
Template.registerHelper 'moneyFormat', (i) ->
  accounting.formatMoney i, symbol: '$'
