Template.userNavBar.onRendered ->
  @$('.ui.rating').rating
    initialRating: 1
    maxRating: 5
  return
Template.userNavBar.helpers navOptions: ->
  selectedLink = Template.instance().data.selectedLink
  [
    'Dashboard'
    'Projects'
    'Portfolio'
    'Settings'
  ].map (option) ->
    {
      name: option
      classes: if option == selectedLink then 'selected' else ''
      href: '/user/' + option.toLowerCase()
    }
