Router.route '/user/settings/', ->
  @layout 'layout'
  @render 'userSettings'
  return

AutoForm.hooks userSettingsForm:
  onSuccess: (formType, doc) ->
    return
  onError: ->
    console.log arguments
    return