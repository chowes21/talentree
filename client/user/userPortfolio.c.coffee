Router.route '/user/portfolio/', ->
  @layout 'layout'
  @render 'userPortfolio'
  return

Template.userPortfolio.created = ->
  @selectedTab = new ReactiveVar('Video')
  return

Template.userPortfolio.helpers
  tabs: ->
    selectedTab = Template.instance().selectedTab.get()
    [
      {
        value: 'Video'
        label: 'Videos'
        classes: if selectedTab == 'Video' then 'active' else ''
      }
      {
        value: 'Image'
        label: 'Images'
        classes: if selectedTab == 'Image' then 'active' else ''
      }
      {
        value: 'Audio'
        label: 'Audio'
        classes: if selectedTab == 'Audio' then 'active' else ''
      }
    ]
  filteredSubmissions: ->
    db.Submissions.find format: Template.instance().selectedTab.get()
  noSubmissions: ->
    false
Template.userPortfolio.events
  'click a': (e) ->
    Template.instance().selectedTab.set $(e.target).data('tab')
    return
  'click #upload': (e) ->
    Session.set 'activeModal', 'upload'
    return
