Template.submit.onCreated ->
  console.log 'Goto: Submit'
  self = this
  Util.setTemplateState this, 'medias': []
  id = Router.current().params.id
  @subscribe 'project', id, ->
    self.state 'project', db.Projects.findOne(id)
    return
  return
Template.submit.helpers {}
Template.submit.events
  uploadImage: (e, t) ->
    files = $('input.file_bag')[0].files
    S3.upload {
      files: files
      path: 'subfolder'
    }, (e, r) ->
      console.log r
      return
    return
  submit: (e, t) ->
    doc = AutoForm.getFormValues('projectSubmission').insertDoc
    doc.projectId = t.state('project')._id
    if !AutoForm.validateForm('projectSubmission')
      return
    console.log 'submit to project: ' + JSON.stringify(doc)
    Meteor.call 'insertSubmission', doc, (err, res) ->
      if err
        console.error 'An error has occured! ' + err.description
      else
        Router.go '/'
      return
    return
