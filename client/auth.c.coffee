Router.route '/complete-signup', ->
  @layout 'layout'
  @render 'completeSignup'
  return
Router.route '/login', (->
  @render 'login'
  return
), name: 'login'
Router.route '/logout', ->
  AccountsTemplates.logout()
  return
