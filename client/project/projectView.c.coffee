Router.route '/project/:id/', ->
  @layout 'layout'
  project = db.Projects.findOne @params.id
  Session.set 'chapp-docid', project._id
  @render 'projectView', data: -> project
  return

Template.projectView.onCreated ->
  @selectedTab = new ReactiveVar('Details')
  return

Template.projectView.onRendered ->
  @autorun (c) ->
    profile = Meteor.user()?.profile
    console.log Template.currentData()?._id
    Session.set 'chapp-username', "#{profile.firstName} #{profile.lastName}"
    return

Template.projectView.helpers
  isSelectedTab: (tab) ->
    selectedTab = Template.instance().selectedTab.get()
    tab == selectedTab
  exampleItems: ->
    examples = @examples
    Util.getSubmissionsInList examples
  userSubmissions: ->
    submissions = db.Projects.findOne(@_id).submissions
    _.map submissions, (s) ->
      user = Meteor.users.findOne(s.user)
      {
        selected: s.selected
        selectedClass: if s.selected then "active" else ""
        user: s.user
        talent: s.talent
        subtalent: s.subtalent
        userName: user.profile.firstName + ' ' + user.profile.lastName
        submissions: Util.getSubmissionsInList(s.submissions)
      }
  selectedUsers: ->
    submissions = db.Projects.findOne(@_id).submissions
    x = _.chain submissions
      .filter (s) -> s.selected
      .map (s) -> 
        _.extend {}, Meteor.users.findOne(s.user).profile, {submittedTalent: s.subtalent}
      .groupBy 'submittedTalent'
      .map (v, k) -> 
        {subtalent: k
        users: v}
      .value()
    x

  tabs: ->
    selectedTab = Template.instance().selectedTab.get()
    [
      {
        value: 'Details'
        label: 'Details'
        classes: if selectedTab == 'Details' then 'active' else ''
      }
      {
        value: 'Submissions'
        label: 'Submissions'
        classes: if selectedTab == 'Submissions' then 'active' else ''
      }
      {
        value: 'Collaborate'
        label: 'Collaborate'
        classes: if selectedTab == 'Collaborate' then 'active' else ''
      }
      {
        value: 'Comments'
        label: 'Comments'
        classes: if selectedTab == 'Comments' then 'active' else ''
      }
    ]
  getIconType: ->
    ICON_TYPES = 
      'Barter': 'comments'
      'Collaborate': 'idea'
      'Bounty': 'dollar'
    ICON_TYPES[@projectType]

Template.projectView.events
  'click a.item': (e, template) ->
    template.selectedTab.set $(e.target).data('tab')
    return
  'click li.collaborator': (e) ->
    Session.set 'activeModal', 'submit'
    Session.set 'selectedTalent', $(e.target).data('talent')
    Session.set 'selectedSubtalent', $(e.target).data('subtalent')
    return
  'click button.select': (e, template) ->
    projectId = template.data._id
    userId = this.user
    console.log this
    Meteor.call('toggleSelectSubmission', projectId, userId, !this.selected)
  'click button.complete-project': (e, template) ->
    db.Projects.update template.data, 
      {$set:
        {completed: true}},
      {validate: false}

